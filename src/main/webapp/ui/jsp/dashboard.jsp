<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Spring Security Example</title>
    <link href="/bootstrap.min.css" rel="stylesheet">
    <script src="/jquery-2.2.1.min.js"></script>
    <script src="/bootstrap.min.js"></script>
</head>
<body>
<div>
    <div class="container" style="margin: 50px">
        <div>
            <form action="/doLogout" method="post">
                <button type="submit" class="btn btn-danger">Log Out</button>
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
            </form>
        </div>
        <div class="row text-center">
            <strong> User Details </strong>
        </div>
        <div class="row" style="border: 1px solid green; padding: 10px">
            <div class="col-md-4 text-center">
                <strong>UserName</strong>
            </div>
            <div class="col-md-4 text-center">
                <strong>FirstName</strong>
            </div>
            <div class="col-md-4 text-center">
                <strong>LastName</strong>
            </div>
        </div>
        <c:forEach var="user" items="${users}">
            <div class="row" style="border: 1px solid green; padding: 10px">
                <div class="col-md-4 text-center">${user.username}</div>
                <div class="col-md-4 text-center">${user.firstname}</div>
                <div class="col-md-4 text-center">${user.lastname}</div>
            </div>
        </c:forEach>

    </div>
</div>
</body>
</html>