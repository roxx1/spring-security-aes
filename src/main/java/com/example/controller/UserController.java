package com.example.controller;

import com.example.model.User;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/dashboard")
    public ModelAndView fetchAllUsers() {
        ModelAndView model = new ModelAndView();
        model.addObject("users", userService.fetchAllUsers());
        model.setViewName("dashboard");
        return model;
    }

    @GetMapping("/users/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public User fetchUser(@PathVariable("userId") Long userId) {
        return userService.fetchUser(userId);
    }

}
