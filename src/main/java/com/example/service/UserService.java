package com.example.service;

import com.example.model.User;

import java.util.List;

public interface UserService {
    User create(User user);

    User fetchUser(Long userId);

    List<User> fetchAllUsers();
}
