package com.example.security;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public final class AES {

    private AES() {
        throw new AssertionError();
    }

    private static byte[] encKey = new byte[]{-32, 74, 61, 28, -125, 118, -124, 85, 37, -23, 88, 110, 32, -76, -55, 74};


    public static String encrypt(String plainText) throws Exception {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(encKey, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal((new BASE64Decoder()).decodeBuffer(plainText));
            return (new BASE64Encoder()).encode(encrypted);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return null;
        }
    }

    public static String decrypt(String encryptedText) throws Exception {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(encKey, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] decrypted = cipher.doFinal((new BASE64Decoder()).decodeBuffer(encryptedText));
            return (new BASE64Encoder()).encode(decrypted);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return null;
        }
    }

}