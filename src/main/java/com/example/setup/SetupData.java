package com.example.setup;

import com.example.model.User;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class SetupData implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        User user1 = new User("ram", "ram", "def", "abcd");
        User user2 = new User("shyam", "shyam", "def", "abcd");
        User user3 = new User("king", "king", "def", "abcd");
        User user4 = new User("kong", "kong", "def", "abcd");

        userService.create(user1);
        userService.create(user2);
        userService.create(user3);
        userService.create(user4);
    }
}
